<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatriculasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matriculas', function (Blueprint $table) {
            $table->increments('idmatriculas');
            $table->integer('cod')->unsigned();
            $table->integer('codestudiante');
            $table->date('fechamatricula');
            $table->integer('numsemestre');
            $table->foreign('cod')
                  ->references('id')
                  ->on('programas_materias')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->foreign('codestudiante')
                  ->references('cedula')
                  ->on('estudiantes')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
            $table->engine='InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matriculas');
    }
}
