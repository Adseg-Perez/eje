<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramasMateriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('programas_materias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('codprograma')->unsigned();
            $table->integer('codmateria')->unsigned();
            $table->foreign('codprograma')
            ->references('idprograma')
            ->on('programas')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->foreign('codmateria')
            ->references('idmateria')
            ->on('materias')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
            $table->engine='InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programas_materias');
    }
}
