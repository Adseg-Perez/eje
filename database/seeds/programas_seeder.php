<?php

use Illuminate\Database\Seeder;

class programas_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::insert("INSERT INTO programas(nombre,numerosemestre) VALUES 
        	('Ingeniería De Sistemas','10'),
        	('Ingeniería Electronica','10'),
        	('Administración De Empresas','10'),
        	('Administración En Salud','10'),
        	('Contaduria','9');");
    }
}
