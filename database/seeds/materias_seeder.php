<?php

use Illuminate\Database\Seeder;

class materias_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::insert("INSERT INTO materias(nombre) VALUES 
        	('Español'),
        	('Ecuaciones'),
        	('Desarrollo Web'),
        	('Ingeniería de software'),
        	('Redes'),
        	('Programación Orientada A Objetos');");
    }
}
