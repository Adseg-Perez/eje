<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Estudiante;
class EstudianteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){
        $estudiantes=Estudiante::all();
        return view('estudiantes.index',['estudiantes'=>$estudiantes]);
    }

    public function create(){
        $estudiantes=Estudiante::all();
        return view('estudiantes.create');
    }

    public function store(Request $request){
        $datos=$request->all();
        $estudiantes = new Estudiante;
        $estudiantes->cedula=$request->input('cedula');
        $estudiantes->nombres=$request->input('nombres');
        $estudiantes->apellidos=$request->input('apellidos');
        $estudiantes->telefono=$request->input('telefono');
        $estudiantes->email=$request->input('email');
        $estudiantes->direccion=$request->input('direccion');
        $estudiantes->genero=$request->input('genero');
        $estudiantes->save();
        return redirect()->route('estudiante.index');
    }

     public function edit($cedula){
        $estudiante=Estudiante::find($cedula);
        return view('estudiantes.edit')->with('estudiante',$estudiante);
    }

     public function update(Request $request,$cedula){
        $estudiante=Estudiante::find($cedula);
        $datos=array();
        $datos['nombres']=$request->input('nombres');
        $datos['apellidos']=$request->input('apellidos');
        $datos['telefono']=$request->input('telefono');
        $datos['email']=$request->input('email');
        $datos['direccion']=$request->input('direccion');
        $datos['genero']=$request->input('genero');
        $estudiante->update($datos);
        return redirect()->route('estudiante.index');

    }

     public function delete($cedula){
        $estudiantes = Estudiante::find($cedula);
        $estudiantes->delete();
        return redirect()->route('estudiante.index');
    }
    //
}