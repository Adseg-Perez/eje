<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Materia;

class MateriaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){
        $materias=Materia::all();
        return view('materias.index',['materias'=>$materias]);
    }

    public function create(){
        $materias=Materia::all();
        return view('materias.create');
    }

    public function store(Request $request){
        $datos=$request->all();
        $materia = new Materia;
        $materia->nombre=$request->input('nombre');
        $materia->save();
        return redirect()->route('materia.index');
    }

    public function edit($idmateria){
        $materia=Materia::find($idmateria);
        return view('materias.edit')->with('materia',$materia);
    }

    public function update(Request $request,$idmateria){
        $materia=Materia::find($idmateria);
        $datos=array();
        $datos['nombre']=$request->input('nombre');
        $materia->update($datos);
        return redirect()->route('materia.index');

    }

    public function delete($idmateria){
        $materia=Materia::find($idmateria);
        $materia->delete();
        return redirect()->route('materia.index');
    }

    //
}