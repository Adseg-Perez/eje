<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Estudiante;
use App\Models\Programa;
use App\Models\ProgramaMateria;
use App\Models\matricula;


class MatriculaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        $matriculas = matricula::all();
        $estudiantes = Estudiante::all();
        return view('matriculas.index')
        ->with('matriculas', $matriculas)
        ->with('estudiantes',$estudiantes);
    }

     public function create()
    {
        $estudiantes=Estudiante::all();
        $programamaterias=ProgramaMateria::all();
        return view('matriculas.create')
        ->with('estudiantes',$estudiantes)
        ->with('programamaterias',$programamaterias);
    }

    public function store(Request $request)
    {
        //$datos=$request->all();
        $materia = new Matricula;
        $materia->cod = $request->input('cod'); 
        $materia->codestudiante =$request->input('codestudiante');
        $materia->fechamatricula = $request->input('fecha');   
        $materia->numsemestre =$request->input('numsemestre');  
        $materia->save();  

        return redirect()->route('matricula.index');
    }

    public function delete($idmatriculas){
        $matricula=Matricula::find($idmatriculas);
        $matricula->delete();
        return redirect()->route('matricula.index');
    }
    //
}