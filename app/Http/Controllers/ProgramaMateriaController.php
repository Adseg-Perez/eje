<?php 


namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Materia;
use App\Models\Programa;
use App\Models\ProgramaMateria;


class ProgramaMateriaController extends Controller
{

    public function index()
    {
        $programas_materias = ProgramaMateria::all();

        return view('programasmaterias.index', ['programas_materias' => $programas_materias]);
    }

    public function create()
    {
        $programas=Programa::all();
        $materias=Materia::all();
        return view('programasmaterias.create')
        ->with('programas',$programas)
        ->with('materias',$materias);
    }

    public function store(Request $request)
    {
        //$datos=$request->all();
        $programa          = new Programa;
        $programa->nombre    = $request->input('nombre'); 
        $programa->numerosemestre  =$request->input('numSemestres');    
        $programa->save();
        
        $materias    = $request->input('mats');
        
        $programa->programas_materias()->attach($materias);      


       
        return redirect()->route('programamateria.index');
    }

    public function edit($id)
    {
       
        
       // $materia = Materia::find($id);
         $programa = Programa::where('idprograma',$id)->first();
          $materias = Materia::all();
        //$materia=$materia->tojson();
        //dd($programa->programas_materias->tojson;
          
        return view('programasmaterias.edit')
        ->with('programa',$programa)
        ->with('materias',$materias);

    }

    public function update(Request $request, $id)
    {
        // $materia = Materia::find($id);
        $programa = Programa::where('idprograma',$id)->firstOrFail();              
         $datos                        = array();
         $datos['nombre']=$request->input('nombre');
         $datos['numerosemestre']=$request->input('numerosemestres');
         $programa->update($datos);
         $programa->programas_materias()->detach();
         $materias    = $request->input('mats');
         $programa->programas_materias()->attach($materias);   

        return redirect()->route('programamateria.index');
    }

    public function delete($id)
    {
        
        //$materia = Materia::find($id);
       $programa = Programa::where('idprograma',$id)->firstOrFail();  
        $programa->programas_materias()->detach();
       $programa->delete();
       return redirect()->route('programamateria.index');
    }

   

}
