<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Materia extends Model 
{
   
   	protected $primaryKey='idmateria';

    protected $table='materias';
    protected $fillable = ['idmateria','nombre'];
    protected $hidden = ['created_at','updated_at','deleted_at'];

    public function programas_materias(){
    	return $this->belongsToMany('\App\Models\Programa','programas_materias','codprograma','codmateria');
    }

}