<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class ProgramaMateria extends Model 
{
   
    protected $primaryKey="id";
    
    protected $table='programas_materias';
    protected $fillable = ['id','codprograma','codmateria'];
    protected $hidden = ['created_at','updated_at','deleted_at'];

    public function programa(){
    	return $this->belongsTo('\App\Models\Programa','codprograma');
    }

    public function materia(){
    	return $this->belongsTo('\App\Models\Materia','codmateria');
    }

    public function matricula(){
        return $this->belongsToMany('\App\Models\Estudiante','matricula','cod','codestudiante');
    }
}