<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Matricula extends Model 
{
    protected $primaryKey='idmatriculas';

    protected $table='matriculas';
    protected $fillable = ['idmatriculas','cod','codestudiante','fechamatricula','numsemestre'];
    protected $hidden = ['created_at','updated_at','deleted_at'];

    public function programa(){
    	return $this->belongsTo('\App\Models\Estudianre','codestudiante');
    }

    public function programa_materia(){
    	return $this->belongsTo('\App\Models\ProgramaMateria','cod');
    }
}