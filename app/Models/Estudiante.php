<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Estudiante extends Model 
{
    protected $primaryKey='cedula';

    protected $table='estudiantes';
    protected $fillable = ['cedula','nombres','apellidos','telefono','email','direccion','genero'];
    protected $hidden = ['created_at','updated_at','deleted_at'];

    public function matricula(){
    	return $this->belongsToMany('\App\Models\ProgramaMateria','matricula','cod','codestudiante');
    }
}