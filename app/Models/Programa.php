<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Programa extends Model 
{
   
   	protected $primaryKey="idprograma";

    protected $table='programas';
    protected $fillable = ['idprograma','nombre','numerosemestre'];
    protected $hidden = ['created_at','updated_at','deleted_at'];

    public function programas_materias(){
    	return $this->belongsToMany('\App\Models\Materia','programas_materias','codprograma','codmateria');
    }
}
?>