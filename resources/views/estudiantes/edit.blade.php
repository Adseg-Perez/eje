@extends('plantilla.plantilla')
@section('titulo','LumenLTE 2| Materia')

@section('contenido')
<div class="box box-info">
  <div class="box-header with-border">
    <h3 class="box-title">Registrar Materias</h3>
  </div>
  <form class="form-horizontal" role="form" method="POST" action="{{ route('estudiante.update',['id'=>$estudiante->cedula]) }}">
    <div class="box-body">
      <input type="hidden" name="_method" value="PATCH">
            <div class="form-group">
              <div class="col-md-2">
                <label for="nombres" class="control-label"><i class="fa fa-user-plus"></i> Nombres</label>
              </div>
              <div class="col-md-9 has-feedback">
                <div class="form-group has-feedback">
                  <input type="text" name="nombres" id="nombres" class="form-control" placeholder="Nombres" value="{{$estudiante->nombres}}">
                  <span class="fa fa-user-plus form-control-feedback"></span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-2">
                <label for="apellidos" class="control-label"><i class="fa fa-user"></i> Apellidos</label>
              </div>
              <div class="col-md-9 has-feedback">
                <div class="form-group has-feedback">
                  <input type="text" name="apellidos" id="apellidos" class="form-control" placeholder="Apellidos" value="{{$estudiante->apellidos}}">
                  <span class="fa fa-user form-control-feedback"></span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-2">
                <label for="telefono" class="control-label"><i class="fa fa-phone-square"></i> Teléfonos</label>
              </div>
              <div class="col-md-9 has-feedback">
                <div class="form-group has-feedback">
                  <input type="tel" name="telefono" id="telefono" class="form-control" placeholder="Telefonos" value="{{$estudiante->telefono}}">
                  <span class="fa  fa-phone-square form-control-feedback"></span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-2">
                <label for="email" class="control-label"><i class="fa fa-envelope"></i> E-mail</label>
              </div>
              <div class="col-md-9 has-feedback">
                <div class="form-group has-feedback">
                  <input type="email" name="email" id="email" class="form-control" placeholder="Correo Electronico" value="{{$estudiante->email}}">
                  <span class="fa fa-envelope form-control-feedback"></span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-2">
                <label for="direccion" class="control-label text-rigth"><i class="fa fa-credit-card"></i> Dirección</label>
              </div>
              <div class="col-md-9 has-feedback">
                <div class="form-group has-feedback">
                  <input type="text" name="direccion" id="direccion" class="form-control" placeholder="Dirección" value="{{$estudiante->direccion}}">
                  <span class="fa fa-credit-card form-control-feedback"></span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-2">
                <label for="genero" class="control-label"><i class="fa  fa-venus-mars"></i> Género</label>
              </div>
              <div class="col-md-9 has-feedback">
                <div class="form-group has-feedback">
                  <select id="genero" name="genero" class="form-control">
                    @if($estudiante->genero == 'masculino')
                      <option value="masculino" selected="">Masculino</option>
                      <option value="femenino">Feminino</option>
                      <option value="otros">Otros</option>
                    @endif
                    @if($estudiante->genero == 'femenino')
                      <option value="masculino">Masculino</option>
                      <option value="femenino" selected="">Feminino</option>
                      <option value="otros">Otros</option>
                    @endif
                    @if($estudiante->genero == 'otros')
                      <option value="masculino">Masculino</option>
                      <option value="femenino">Feminino</option>
                      <option value="otros" selected="">Otros</option>
                    @endif
                  </select>
                </div>
              </div>
            </div>
    </div>
    <div class="box-footer">
      <a href="{{route('estudiante.index')}}" class="btn btn-default btn-sm btn-adon"><i class="glyphicon glyphicon-remove"></i> cancelar</a>
      <button type="submit"  class="btn btn-info pull-right">Edit</button>
    </div>
  </form>
  
</div>
@endsection

@section('menu')
@include('plantilla.menu')
@endsection