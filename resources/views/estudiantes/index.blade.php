
@extends('plantilla.plantilla')
@section('titulo','LumenLTE 2| Materia')

@section('contenido')
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Listado de materias</h3>
    <a href="{{ route('estudiante.create') }}" class="btn btn-sm btn-success btn-addon"><i class="glyphicon glyphicon-plus"></i> crear</a>
  </div>
  <div class="box-body">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th style="width: 150px;">cedula</th>
          <th>Nombres</th>
          <th>Telefonos</th>
          <th>Email</th>
          <th>Direccion</th>
          <th>Genero</th>
          <th style="width: 150px;">Opciones</th>
          <th>jkahsjkdas</th>
    
        </tr>
      </thead>
      <tbody>
        @foreach($estudiantes as $estudiante)
          <tr>
            <td>{{$estudiante->cedula}}</td>
            <td>{{$estudiante->nombres}} {{$estudiante->apellidos}}</td>
            <td>{{$estudiante->telefono}}</td>
            <td>{{$estudiante->email}}</td>
            <td>{{$estudiante->direccion}}</td>
            <td>{{$estudiante->genero}}</td>
            <td><center>ijsdhsj       </td>
            <td>
              <div> </div>
              <a href="{{route('estudiante.edit',['id'=>$estudiante->cedula])}}" class="btn btn-info btn-sm">Editar</a>
              <a href="{{route('estudiante.delete',['id'=>$estudiante->cedula])}}" class="btn btn-danger btn-sm">Eliminar</a>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection

@section('menu')
@include('plantilla.menu')
@endsection