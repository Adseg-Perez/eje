@extends('plantilla.plantilla')
@section('titulo','proyecto-lumen')
@section('contenido')
     <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title">Bienvenidos a Lumen </h3>
          </div>
          <div class="box-body">
            Estoy feliz de aprenderlo <i class="fa fa-fw fa-odnoklassniki"></i>
          </div>
    </div>

@endsection

@section('menu')
  @include('plantilla.menu')
@endsection