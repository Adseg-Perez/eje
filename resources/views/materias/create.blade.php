@extends('plantilla.plantilla')
@section('titulo','LumenLTE 2| Materia')

@section('contenido')
<div class="box box-info">
  <div class="box-header with-border">
    <h3 class="box-title">Registrar Materias</h3>
  </div>
  <form class="form-horizontal" role="form" method="POST" action="{{route('materia.store')}}">
    <div class="box-body">
      <div class="form-group">
        <label for="inputName" class="col-sm-2 control-label">Nombre Materia:</label>
        <div class="col-sm-10">
          <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre de la materia">
        </div>
      </div>
    </div>
    <div class="box-footer">
      <a href="{{route('materia.index')}}" class="btn btn-default btn-sm btn-adon"><i class="glyphicon glyphicon-remove"></i> cancelar</a>
      <button type="submit"  class="btn btn-info pull-right">Guardar</button>
    </div>
  </form>
  
</div>
@endsection

@section('menu')
@include('plantilla.menu')
@endsection