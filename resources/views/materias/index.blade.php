
@extends('plantilla.plantilla')
@section('titulo','LumenLTE 2| Materia')

@section('contenido')
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Listado de materias</h3>
    <a href="{{ route('materia.create') }}" class="btn btn-sm btn-success btn-addon"><i class="glyphicon glyphicon-plus"></i> crear</a>
  </div>
  <div class="box-body">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th style="width: 60px;">#</th>
          <th>name</th>
          <th style="width: 150px;">options</th>
        </tr>
      </thead>
      <tbody>
        @foreach($materias as $materia)
          <tr>
            <td>{{$materia->idmateria}}</td>
            <td>{{$materia->nombre}}</td>
            <td>
              <a href="{{route('materia.edit',['id'=>$materia->idmateria])}}" class="btn btn-info btn-sm">Editar</a>
              <a href="{{route('materia.delete',['id'=>$materia->idmateria])}}" class="btn btn-danger btn-sm">Eliminar</a>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection

@section('menu')
@include('plantilla.menu')
@endsection