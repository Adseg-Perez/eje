
@extends('plantilla.plantilla')
@section('titulo','LumenLTE 2| Materia')

@section('contenido')
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Listado de matriculas</h3>
    <a href="{{ route('matricula.create') }}" class="btn btn-sm btn-success btn-addon"><i class="glyphicon glyphicon-plus"></i> crear</a>
  </div>
  <div class="box-body">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th style="width: 150px;">#</th>
          <th>program</th>
          <th>Students</th>
          <th>Date</th>
          <th>Number</th>
          <th style="width: 150px;">Opciones</th>
        </tr>
      </thead>
      <tbody>
        @foreach($matriculas as $matricula)
          <tr>
            <td>{{$matricula->idmatriculas}}</td>
            <td>{{$matricula->cod}}</td>
            @foreach($estudiantes as $estudiante)
              @if($matricula->codestudiante == $estudiante->cedula)
                <td>{{$estudiante->nombres}} {{$estudiante->apellidos}}</td>
              @endif
            @endforeach
            <td>{{$matricula->fechamatricula}}</td>
            <td>{{$matricula->numsemestre}}</td>
            <td>
              <a href="{{route('matricula.delete',['id'=>$matricula->idmatriculas])}}" class="btn btn-danger btn-sm">Eliminar</a>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection

@section('menu')
@include('plantilla.menu')
@endsection