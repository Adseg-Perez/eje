@extends('plantilla.plantilla')
@section('titulo','LumenLTE 2| Materia')

@section('contenido')
<div class="box box-info">
  <div class="box-header with-border">
    <h3 class="box-title">Registrar Matricula</h3>
  </div>
  <form class="form-horizontal" role="form" method="POST" action="{{route('matricula.store')}}">
    <div class="box-body">
            
            <div class="form-group">
              <div class="col-md-2">
                <label for="cod" class="control-label"><i class="fa  fa-venus-mars"></i> Codigo</label>
              </div>
              <div class="col-md-9 has-feedback">
                <div class="form-group has-feedback">
                  <select id="cod" name="cod" class="form-control">
                    <option disabled="" selected="">==Seleccione==</option>
                      @foreach($programamaterias as $programamateria)
                        <option value="{{$programamateria->id}}">{{$programamateria->id}}</option>
                      @endforeach        
                  </select>
                </div>
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-2">
                <label for="codestudiante" class="control-label"><i class="fa  fa-venus-mars"></i> Estudiante</label>
              </div>
              <div class="col-md-9 has-feedback">
                <div class="form-group has-feedback">
                  <select id="codestudiante" name="codestudiante" class="form-control">
                    <option value="0" disabled="" selected="">==Seleccione==</option>
                     @foreach($estudiantes as $estudiante)
                        <option value="{{$estudiante->cedula}}">{{$estudiante->nombres}} {{$estudiante->apellidos}}</option>
                     @endforeach         
                  </select>
                </div>
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-2">
                <label for="apellidos" class="control-label"><i class="fa fa-user"></i> fecha</label>
              </div>
              <div class="col-md-9 has-feedback">
                <div class="form-group has-feedback">
                  <input type="date" name="fecha" id="fecha" class="form-control" placeholder="fecha">
                  <span class="fa fa-user form-control-feedback"></span>
                </div>
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-2">
                <label for="telefono" class="control-label"><i class="fa fa-phone-square"></i> Numero Semestre </label>
              </div>
              <div class="col-md-9 has-feedback">
                <div class="form-group has-feedback">
                  <input type="number" name="numsemestre" id="numsemestre" class="form-control" placeholder="Numero semestre">
                  <span class="fa  fa-phone-square form-control-feedback"></span>
                </div>
              </div>
            </div>
    </div>
    <div class="box-footer">
      <a href="{{route('matricula.index')}}" class="btn btn-default btn-sm btn-adon"><i class="glyphicon glyphicon-remove"></i> cancelar</a>
      <button type="submit"  class="btn btn-info pull-right">Guardar</button>
    </div>
  </form>
  
</div>
@endsection

@section('menu')
@include('plantilla.menu')
@endsection