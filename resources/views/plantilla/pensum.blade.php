<!--Formulario pensum-->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border"><!--cabecera-->
          <h2 class="box-title">
            <i class="fa  fa-edit"></i>
            <b>Pensum</b>
          </h2>
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
          <br>
        </div><!--fin cabecera-->
        <form class="programa" action="" method="POST" id="programa"><!--formulario-->
          <div class="box-body">
            <div class="form-group">
              <div class="col-md-12">
                <div class="col-md-6">
                  <label for="codprograma" class="control-label"><i class="fa fa-file-word-o"></i> Programa</label>
                </div>
                <div class="col-md-6">
                  <label for="codmateria" class="control-label"><i class="fa fa-list"></i> materia</label>
                </div>
              </div>
              <div class="col-md-12">
                <div class="col-md-6">
                  <select class="form-control" id="codprograma" name="codprograma">
                    <option disabled selected>==seleccionar==</option>
                  </select>
                </div>
                <div class="col-md-6">
                  <select class="form-control" id="codmateria" name="codmateria">
                    <option disabled selected>==seleccionar==</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="form-group">
              <br>
              <br>
            </div>
          </div>
        </form><!--fin formulario-->
        <div class="box-footer"><!--piecera-->
          <button type="submit" class="btn btn-info pull-right"><i class="fa fa-mail-forward"></i>Registrar</button>
        </div><!--fin piecera-->
      </div>
    </div>
  </div>
</section>