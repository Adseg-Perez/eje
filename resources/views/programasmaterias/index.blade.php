@extends('plantilla.plantilla')
@Section('contenido')
<div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Asignacion de materias a programas</h3>

            <a href="{{ route('programamateria.create') }}" class="btn btn-sm btn-success btn-addon"><i class="glyphicon glyphicon-plus"></i>Crear</a>      
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
              	<thead>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Program</th>
                  <th>class</th>
                  <th style="width: 140px">Opciones</th>
                </tr>
             </thead>
              <tbody>
                            

              	 @forelse($programas_materias as $progra_materia)
                    <tr>
                         <td>{{ $progra_materia->id }}</td>
                        <td>{{ $progra_materia->programa->nombre }}</td>
                        <td>{{ $progra_materia->materia->nombre}}</td>                     
                        <td>
                            <a href="{{ route('programamateria.edit', ['id' => $progra_materia->programa->idprograma]) }}" class="btn btn-info btn-sm">Editar</a>
                            <a href="{{ route('programamateria.delete', ['id' => $progra_materia->programa->idprograma]) }}" class="btn btn-danger btn-sm">Eliminar</a>
                        </td>
                    </tr>
                     @empty
                     <tr>
                      <td colspan="4" align="center">
                        No hay datos
                      </td>
                     </tr>
                 @endforelse 
              </tbody>
             </table>
            </div>
            <!-- /.box-body -->
          
          </div>
@endSection
@Section('menu')
@include('plantilla.menu')
@endSection