@extends('plantilla.plantilla')
@Section('contenido')
<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Registrar Materias a programa</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" role="form" method="POST" action="{{ route('programamateria.store') }}">
              <div class="box-body">
                <div class="form-group">

                  <div class="row">
                    <div class="col-md-8">
                      <label for="inputName" class="col-sm-2 control-label">Programa:</label>
                      <div class="col-sm-10">                    
                      <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre del programa">                  
                      </div>
                  </div>
                   <div class="col-md-4">
                      <label for="inputName" class="col-sm-4 control-label"># Semetres:</label>
                      <div class="col-sm-8">                    
                      <input type="text" name="numSemestres" class="form-control" id="nombre" placeholder="Numero de Semetres del programa">                  
                      </div>
                  </div>

                </div>
                </div>

                <div class="form-group">
                  <label for="inputName" class="col-sm-2 control-label">Nombre de la materia:</label>
                  <div class="col-sm-10">
                    <select multiple name="mats[]" class="form-control">
                    @foreach($materias as $materia)
                       <option value='{{$materia->idmateria}}'>{{$materia->nombre}}</option>
                    @endforeach
                  </select>
                  </div>
                </div>

                
            
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{{route('programamateria.index')}}" class="btn btn-default btn-sm btn-addon"><i class="glyphicon glyphicon-remove"></i>Cancel</a>
                <button type="submit" class="btn btn-info pull-right">Guardar</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
@endSection
@Section('menu')
@include('plantilla.menu')
@endSection
