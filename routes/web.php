<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/*$router->get('/', function () use ($router) {
    return $router->app->version();
});
*/


$router->get('/',['uses'=>'InicioController@index',]);

$router->get('materia',['as'=>'materia.index', 'uses'=>'MateriaController@index',]);
$router->get('materia/create',['as'=>'materia.create', 'uses'=>'MateriaController@create',]);
$router->post('materia',['as'=>'materia.store', 'uses'=>'MateriaController@store',]);
$router->get('materia/{id}/edit',['as'=>'materia.edit', 'uses'=>'MateriaController@edit',]);
$router->patch('materia/{id}',['as'=>'materia.update', 'uses'=>'MateriaController@update',]);
$router->get('materia/{id}/delete',['as'=>'materia.delete', 'uses'=>'MateriaController@delete',]);


$router->get('programamateria',['as'=>'programamateria.index', 'uses'=>'ProgramaMateriaController@index',]);
$router->get('programamateria/create',['as'=>'programamateria.create', 'uses'=>'ProgramaMateriaController@create',]);
$router->post('programamateria',['as'=>'programamateria.store', 'uses'=>'ProgramaMateriaController@store',]);
$router->get('programamateria/{id}/edit',['as'=>'programamateria.edit', 'uses'=>'ProgramaMateriaController@edit',]);
$router->patch('programamateria/{id}',['as'=>'programamateria.update', 'uses'=>'ProgramaMateriaController@update',]);
$router->get('programamateria/{id}/delete',['as'=>'programamateria.delete', 'uses'=>'ProgramaMateriaController@delete',]);

$router->get('estudiante',['as'=>'estudiante.index', 'uses'=>'EstudianteController@index',]);
$router->get('estudiante/create',['as'=>'estudiante.create', 'uses'=>'EstudianteController@create',]);
$router->post('estudiante',['as'=>'estudiante.store', 'uses'=>'EstudianteController@store',]);
$router->get('estudiante/{id}/edit',['as'=>'estudiante.edit', 'uses'=>'EstudianteController@edit',]);
$router->patch('estudiante/{id}',['as'=>'estudiante.update', 'uses'=>'EstudianteController@update',]);
$router->get('estudiante/{id}/delete',['as'=>'estudiante.delete', 'uses'=>'EstudianteController@delete',]);

$router->get('matricula',['as'=>'matricula.index', 'uses'=>'MatriculaController@index',]);
$router->get('matricula/create',['as'=>'matricula.create', 'uses'=>'MatriculaController@create',]);
$router->post('matricula',['as'=>'matricula.store', 'uses'=>'MatriculaController@store',]);
$router->get('matricula/{id}/edit',['as'=>'matricula.edit', 'uses'=>'MatriculaController@edit',]);
$router->get('matricula/{id}/delete',['as'=>'matricula.delete', 'uses'=>'MatriculaController@delete',]);
